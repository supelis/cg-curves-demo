#ifndef CURVE2D_H
#define CURVE2D_H

#define DEFAULT_POINT_COUNT 10

#include <QList>
#include <QPointF>
#include <QRectF>
#include <QPainterPath>
#include <QVector3D>

class Curve2D
{
    QList<double> x, y;
    QList<QPointF> points;
    QList<QPointF> controlPolygon;
    QPointF start;
    QPointF end;
    unsigned int curveType;
    unsigned int drawPointCount;
    bool closed;
    bool rationalAlgorithm;
    bool rationalDeCasteljau;

private:
    void addPoint(QPointF point);

    QPointF getPointStep(QPointF startPoint, QPointF endPoint,double t);
    QVector3D getPointStep(QVector3D startPoint, QVector3D endPoint, double t);
    QList<QPointF> reducePoints(QList<QPointF> allPoints, double t);
    QPointF reduceToPoint(QList<QPointF> polygon, double t);
    QList<QPointF> calculateBezierCurve(QList<QPointF> polygon, int pointCount, double tStart, double tStop);
    QList<QPointF> calculateBezierCurve(QList<QPointF> polygon, int pointCount);
    QList<QPointF> subCurvePoints(double tStart, double tStop);
    QList<QPointF> calculateDeCasteljau(QList<QPointF> polygon, int steps);
    QList<QVector3D> calculateDeCasteljau(QList<QVector3D> polygon, int steps);
    QList<QPointF> getDeCasteljauStep(QList<QPointF> polygon);
    QList<QVector3D> getDeCasteljauStep(QList<QVector3D> polygon);
    Curve2D subCurve(bool firstHalf);
    QPointF CRDerivative(QPointF p1, QPointF p2);
    QList<QPointF> CROpenFirstSubControlPolygon(QPointF p0, QPointF p1, QPointF p2);
    QList<QPointF> CROpenLastSubControlPolygon(QPointF p0, QPointF p1, QPointF p2);
    QList<QPointF> CRSubControlPolygon(QPointF p1, QPointF p2, QPointF p3, QPointF p4);
    QPointF BODerivative(QPointF p, QPointF pPrevious, QPointF pNext, double delta, double deltaPrevious);
    QPointF getBOMidPoint(QPointF z0, QPointF _b0, QPointF _b4, QPointF b4);
    QList<QPointF> ISSubControlPolygon(QPointF z0, QPointF _b0, QPointF _b4, QPointF b4, QPointF z1);
    QList<QPointF> BOOpenFirstSubControlPolygon(QPointF p0, QPointF p1, QPointF p2);
    QList<QPointF> BOOpenLastSubControlPolygon(QPointF p0, QPointF p1, QPointF p2);
    QList<QPointF> BOSubControlPolygon(QPointF p1, QPointF p2, QPointF p3, QPointF p4);
    QList<QPointF> ChaikinDividePolygon(QList<QPointF> curvePolygon);
    QList<QPointF> ChaikinDividePolygon3rd(QList<QPointF> curvePolygon);
    double BOGetDistance(QPointF p1, QPointF p2);
    int getListIndex(int index, int listLength);
    QList<QPointF> calculateRationalBezier(QList<QPointF> polygon, int pointCount);
    double calculateBernstein(int i, int n, double t);
    QList<QVector3D> convertPolygonTo3D(QList<QPointF> polygon, QList<double> weights);
    QList<QPointF> perspectiveDivide(QList<QVector3D> polygon);
    QList<QPointF> calculateRationalDeCasteljau(QList<QPointF> polygon, int steps);

    bool shellsCollide(QRectF shell1, QRectF shell2);

public:
    static const unsigned int TYPE_BEZIER;
    static const unsigned int TYPE_SPLINE;
    static const unsigned int TYPE_SPLINE_2;
    static const unsigned int TYPE_CATMULL_ROM;
    static const unsigned int TYPE_BESSEL_OVERHAUSER;
    static const unsigned int TYPE_CHAIKIN;
    static const unsigned int TYPE_CHAIKIN_2;
    static const unsigned int TYPE_RATIONAL_BEZIER;
    static const unsigned int TYPE_INTERPOLATING;

    static const unsigned int DISTANCE_EUCLIDEAN;
    static const unsigned int DISTANCE_CITY_BLOCK;
    static const unsigned int DISTANCE_CHEBYSHEV;

    unsigned int algorithmSteps;
    unsigned int distanceMetric;
    double distanceAlfa;
    double rationalWeight;
    QList<double> rationalWeights;

    bool isCalculated;

    Curve2D();
    void addPoints(QList<QPointF> newPoints);
    void setStart(QPointF startPoint);
    void setEnd(QPointF endPoint);
    void addControlPoint(QPointF point);
    void addControlPoint(QPointF point, bool nearest);
    void appendControlPoint(QPointF point);
    void removeLastControlPoint(void);
    QList<double> getXCoords(void);
    QList<double> getYCoords(void);
    void calculateCurve(void);
    void updateControlPoint(int index, QPointF newPoint);
    void setDrawPointCount(int count);
    int getNearestCPIndex(QPointF point);
    QList<QPointF> getPoints(void);
    QList<QPointF> getNearestPoints(QPointF point);
    QPainterPath getCurvePath(void);
    QPainterPath getControlPolygonPath(int selectedIndex);
    QPainterPath getControlPolygonPath(void);
    QPointF pointMirror(QPointF point, QPointF center);
    QList<QPointF> getControlPoints(void);
    void setType(unsigned int type);
    unsigned int getType(void);
    void unsetCurve(void);
    void setClosed(bool isClosed);
    QList<QPointF> findCollisions(Curve2D curve1, Curve2D collisionCurve, double epsilon);
    QList<QPointF> findCollisions(Curve2D curve1, Curve2D collisionCurve);
    QRectF getCurveShell(void);
    bool curvesCollide(Curve2D collisionCurve);
    void increaseCurveDegree(void);
    void setDistanceMetric(unsigned int metric);
    void setDistanceAlfa(double alfa);
    void setAlgorithmSteps(unsigned int stepCount);
    void setRationalWeight(double weight);
    void setRationalWeight(int index, double weight);
    double getRationalWeight(int index);
    void setRationalDeCasteljau(bool enabled);

};

#endif // CURVE2D_H
