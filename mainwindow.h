#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
    void on_checkBox_clicked(bool checked);

    void on_checkBox_2_clicked(bool checked);

    void on_draw_c2_spline_clicked();

    void on_draw_c3_spline_clicked();

    void on_draw_bo_spline_clicked();

    void on_draw_chaikin_clicked();

    void on_draw_rational_bezier_clicked();

    void on_algorithmStepsInput_valueChanged(int arg1);

    void on_rationalWeightInput_valueChanged(int value);

    void on_complete_draw_clicked();

private:
    Ui::MainWindow *ui;
    bool drawingActive = false;
    void forceControlPolygon(void);
    void executeCurveDraw(unsigned int curveType);
    void resetControlsForDrawing(bool enableAlgSteps, bool enableWeightSlider);
};

#endif // MAINWINDOW_H
