#ifndef UTILITIES_H
#define UTILITIES_H

#include <QPoint>
#include <QPointF>
#include <QVector3D>
#include <QList>

bool checkPointClick(QPoint click, QPoint center, double radius);
bool checkPointClick(QPointF click, QPointF center, double radius);
void itoa(int n, char s[]);
void reverse(char s[]);
int strlen(char s[]);
double maxValue(double a, double b);
double euclideanDistance(QPointF point1, QPointF point2);
double chebyshevDistance(QPointF point1, QPointF point2);
double cityBlockDistance(QPointF point1, QPointF point2);
int factorial(int n);
QList<QPointF> perspectiveDivide(QList<QVector3D> polygon);
QPointF perspectiveDivide(double x, double y, double z);
std::string pointToString(QPointF p);

#endif // UTILITIES_H
