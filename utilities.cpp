#include "utilities.h"

bool checkPointClick(QPoint click, QPoint center, double radius) {
    if(click.x() > (center.x() - radius) && click.x() < (center.x() + radius)) {
        if(click.y() > (center.y() - radius) && click.y() < (center.y() + radius)) {
            return true;
        }
    }
    return false;
}

bool checkPointClick(QPointF click, QPointF center, double radius) {
    if(click.x() > (center.x() - radius) && click.x() < (center.x() + radius)) {
        if(click.y() > (center.y() - radius) && click.y() < (center.y() + radius)) {
            return true;
        }
    }
    return false;
}


/* itoa:  convert n to characters in s */
void itoa(int n, char s[])
{
    int i, sign;

    if ((sign = n) < 0)  /* record sign */
        n = -n;          /* make n positive */
    i = 0;
    do {       /* generate digits in reverse order */
        s[i++] = n % 10 + '0';   /* get next digit */
    } while ((n /= 10) > 0);     /* delete it */
    if (sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    reverse(s);
}

/* reverse:  reverse string s in place */
void reverse(char s[])
{
    int i, j;
    char c;

    for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

/* strlen: return length of s */
int strlen(char s[])
{
    int i = 0;
    while (s[i] != '\0')
        ++i;
    return i;
}

double maxValue(double a, double b) {
    return (a < b) ? b : a;
}

double euclideanDistance(QPointF point1 , QPointF point2) {
    return sqrt(pow(point1.x() - point2.x(), 2) + pow(point1.y() - point2.y(), 2));
}

double chebyshevDistance(QPointF point1 , QPointF point2) {
    return maxValue(fabs(point1.x() - point2.x()), fabs(point1.y() - point2.y()));
}

double cityBlockDistance(QPointF point1, QPointF point2) {
    return (fabs(point1.x() - point2.x()) + fabs(point1.y() - point2.y()));
}

int factorial(int n) {
    return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
}

QList<QPointF> perspectiveDivide(QList<QVector3D> polygon) {
    QList<QPointF> reducedPolygon;
    for(QVector3D &vector : polygon) {
        reducedPolygon.append(QPointF(vector.x() / vector.z(), vector.y() / vector.z()));
    }
    return reducedPolygon;
}

QPointF perspectiveDivide(double x, double y, double z) {
    return QPointF(x / z, y / z);
}

std::string pointToString(QPointF p) {
    return std::to_string(p.x()) + 'x' + std::to_string(p.y());
}

