#include "curve2d.h"
#include "utilities.h"
#include <iostream>
#include <cmath>

const unsigned int Curve2D::TYPE_BEZIER = 0;
const unsigned int Curve2D::TYPE_SPLINE = 1;
const unsigned int Curve2D::TYPE_SPLINE_2 = 2;
const unsigned int Curve2D::TYPE_CATMULL_ROM = 3;
const unsigned int Curve2D::TYPE_BESSEL_OVERHAUSER = 4;
const unsigned int Curve2D::TYPE_CHAIKIN = 5;
const unsigned int Curve2D::TYPE_CHAIKIN_2 = 6;
const unsigned int Curve2D::TYPE_RATIONAL_BEZIER = 7;
const unsigned int Curve2D::TYPE_INTERPOLATING = 8;

const unsigned int Curve2D::DISTANCE_EUCLIDEAN = 0;
const unsigned int Curve2D::DISTANCE_CITY_BLOCK = 1;
const unsigned int Curve2D::DISTANCE_CHEBYSHEV = 2;

Curve2D::Curve2D() {
    setDrawPointCount(DEFAULT_POINT_COUNT);
    setType(TYPE_SPLINE);
    setClosed(false);
    distanceMetric = Curve2D::DISTANCE_EUCLIDEAN;
    distanceAlfa = 1;
    algorithmSteps = 5;
    isCalculated = false;
    rationalWeight = 1;
    rationalWeights.append(1);
    rationalAlgorithm = true;
}

 void Curve2D::setDrawPointCount(int count) {
     drawPointCount = count;
 }

void Curve2D::addPoint(QPointF point) {
    points.append(point);
    x.append((double) point.x());
    y.append((double) point.y());
}

void Curve2D::addPoints(QList<QPointF> newPoints) {
    for(int i = 0; i < newPoints.length(); i++) {
        addPoint(newPoints[i]);
    }
}

QList<double> Curve2D::getXCoords() {
    return x;
}

QList<double> Curve2D::getYCoords() {
    return y;
}

void Curve2D::setStart(QPointF startPoint) {
    start = startPoint;
    if(controlPolygon.length() > 0) {
        controlPolygon[0] = startPoint;
    } else {
        controlPolygon.prepend(startPoint);
    }
}

void Curve2D::setEnd(QPointF endPoint) {
    end = endPoint;
    if(controlPolygon.length() > 1) {
        controlPolygon.replace(controlPolygon.length() - 1, endPoint);
    } else {
        controlPolygon.append(endPoint);
    }
}

QList<QPointF> Curve2D::getNearestPoints(QPointF point) {
    int nearestIndex = getNearestCPIndex(point);
    QList<QPointF> nearestPoints;
    nearestPoints.append(controlPolygon.at(nearestIndex));
    if(nearestIndex != 0) {
        nearestPoints.append(controlPolygon.at(nearestIndex - 1));
    }
    return nearestPoints;
}

int Curve2D::getNearestCPIndex(QPointF point) {
    double distances[controlPolygon.length()];
    int i, nearestIndex = 1,
            pointCount = controlPolygon.length() - 1;
    for(i = 0; i <= pointCount; i++) {
        distances[i] = euclideanDistance(point, controlPolygon[i]);
    }
    double distance = distances[1];
    for(i = 1; i < pointCount; i++) {
        if(distance > distances[i]) {
            distance = distances[i];
            nearestIndex = i;
        }
    }
    if(distances[nearestIndex - 1] > distances[nearestIndex + 1]) {
        return nearestIndex + 1;
    } else {
        return nearestIndex;
    }
}

void Curve2D::addControlPoint(QPointF point) {
    int nearestIndex = getNearestCPIndex(point);
    controlPolygon.insert(nearestIndex, point);
    rationalWeights.insert(nearestIndex, rationalWeight);
}

void Curve2D::addControlPoint(QPointF point, bool nearest) {
    if(nearest) {
        addControlPoint(point);
    } else {
        if(controlPolygon.length() > 1) {
            QPointF buffer = controlPolygon.last();
            controlPolygon.removeLast();
            controlPolygon.append(point);
            controlPolygon.append(buffer);
        } else {
            controlPolygon.append(point);
        }
        rationalWeights.append(rationalWeight);
    }
}

void Curve2D::appendControlPoint(QPointF point) {
    controlPolygon.append(point);
}

QList<QPointF> Curve2D::getControlPoints() {
    return controlPolygon;
}

void Curve2D::setDistanceMetric(unsigned int metric) {
    distanceMetric = metric;
}

void Curve2D::setDistanceAlfa(double alfa) {
    distanceAlfa = alfa;
}


QPointF Curve2D::getPointStep(QPointF startPoint, QPointF endPoint, double t) {
    return (((1 - t) * startPoint) + (t * endPoint));
}

QVector3D Curve2D::getPointStep(QVector3D startPoint, QVector3D endPoint, double t) {
    return (((1 - t) * startPoint) + (t * endPoint));
}

QPointF Curve2D::reduceToPoint(QList<QPointF> polygon, double t) {
    QList<QPointF> calculatedPoints;
    calculatedPoints = reducePoints(polygon, t);
    while(calculatedPoints.length() > 1) {
        calculatedPoints = reducePoints(calculatedPoints, t);
    }
    return calculatedPoints[0];
}

/**
 * @brief Curve2D::calculateRationalBezier BUG RESULTS -> DO NOT USE
 * @param polygon
 * @param pointCount
 * @return
 */
QList<QPointF> Curve2D::calculateRationalBezier(QList<QPointF> polygon, int pointCount) {
    int n = polygon.length() - 1;
    double t, tStep = (1 / (double) (pointCount - 1));
    QList<QPointF> newPoints;
    for(int y = 0; y < pointCount; y++) {
        t = tStep * y;
        QPointF newPoint = calculateBernstein(0, n, t) * polygon.first();
        double denominator = calculateBernstein(0, n, t);
        for(int i = 1; i <= n;i++) {
            newPoint += (calculateBernstein(i, n, t) * polygon[i] * (n == i ? 1 : rationalWeights.at(i)));
            denominator += calculateBernstein(i, n, t) * (n == i ? 1 : rationalWeights.at(i));
        }
        newPoints.append(newPoint / denominator);
    }
    return newPoints;
}

QList<QPointF> Curve2D::calculateBezierCurve(QList<QPointF> polygon, int pointCount, double tStart, double tStop) {
    if(tStart == 0 && tStop == 1) {
        return calculateDeCasteljau(polygon, algorithmSteps);
    } else {
        double t,
                step = ((tStop - tStart) / (double) (pointCount - 1));
        QList<QPointF> calculatedPoints;
        for(int i = 0; i < pointCount; i++) {
            t = tStart + (i * step);
            calculatedPoints.append(reduceToPoint(polygon, t));
        }
        return calculatedPoints;
    }
}


QList<QPointF> Curve2D::calculateBezierCurve(QList<QPointF> polygon, int pointCount) {
    return calculateBezierCurve(polygon, pointCount, 0, 1);
}

double Curve2D::calculateBernstein(int i, int n, double t) {
    double var = (double) ((double) factorial(n) / ((double) factorial(i) * (double) factorial(n - i)));
    return (var * (pow((1 - t), (n - i)) * pow(t, i)));
}



QList<QPointF> Curve2D::getDeCasteljauStep(QList<QPointF> polygon) {
    QList<QPointF> newPoints;
    QPointF lastPoint, currentPoint;
    newPoints.prepend(polygon.first());
    for(int i = 1; i < polygon.length(); i++) {
        if(i == 1) {
            lastPoint = getPointStep(polygon[i - 1], polygon[i], 0.5);
            newPoints.append(lastPoint);
        } else {
            currentPoint = getPointStep(polygon[i - 1], polygon[i], 0.5);
            newPoints.append(getPointStep(lastPoint, currentPoint, 0.5));
            newPoints.append(currentPoint);
            lastPoint = currentPoint;
        }
    }
    newPoints.append(polygon.last());
    return newPoints;
}

QList<QVector3D> Curve2D::getDeCasteljauStep(QList<QVector3D> polygon) {
    QList<QVector3D> newPoints;
    QVector3D lastPoint, currentPoint;
    newPoints.prepend(polygon.first());
    for(int i = 1; i < polygon.length(); i++) {
            if(i == 1) {
                lastPoint = getPointStep(polygon[i - 1], polygon[i], 0.5);
                newPoints.append(lastPoint);
            } else {
                currentPoint = getPointStep(polygon[i - 1], polygon[i], 0.5);
                newPoints.append(getPointStep(lastPoint, currentPoint, 0.5));
                newPoints.append(currentPoint);
                lastPoint = currentPoint;
            }
    }
    newPoints.append(polygon.last());
    return newPoints;
}

QList<QPointF> Curve2D::calculateDeCasteljau(QList<QPointF> polygon, int steps) {
    QList<QPointF> calculatedPoints = polygon;
    for(int i = 0; i < steps; i++) {
        calculatedPoints = getDeCasteljauStep(calculatedPoints);
    }
    return calculatedPoints;
}

double maxDistance(QList<QVector3D> polygon) {
    double maxDistance = -INFINITY, curDistance;
    for(int i = 1; i < polygon.length(); i++) {
        curDistance = euclideanDistance(polygon[i-1].toPointF(), polygon[i].toPointF());
        if(curDistance > maxDistance) {
            maxDistance = curDistance;
        }
    }
    return maxDistance;
}

QList<QVector3D> Curve2D::calculateDeCasteljau(QList<QVector3D> polygon, int steps) {
    QList<QVector3D> calculatedPoints = polygon;
    for(int i = 0; i < steps; i++) {
        calculatedPoints = getDeCasteljauStep(calculatedPoints);
    }
   /* double alfaDistance = 2; //pixels
    bool stop = false;
    while(maxDistance()) {

    }
*/
    return calculatedPoints;
}




QList<QVector3D> Curve2D::convertPolygonTo3D(QList<QPointF> polygon, QList<double> weights) {
    QList<QVector3D> threeDimensionalPolygon;
    for(int i = 0; i < polygon.length(); i++) {
        if(i == 0 || i == (polygon.length() - 1)) {
            threeDimensionalPolygon.append(QVector3D(polygon[i].x(), polygon[i].y(), 1));
        } else {
            threeDimensionalPolygon.append(QVector3D((polygon[i].x() * weights[i]), (polygon[i].y() * weights[i]), weights[i]));
        }
    }
    return threeDimensionalPolygon;
}

QList<QPointF> Curve2D::perspectiveDivide(QList<QVector3D> polygon) {
    QList<QPointF> reducedPolygon;
    for(QVector3D &vector : polygon) {
        reducedPolygon.append(QPointF(vector.x() / vector.z(), vector.y() / vector.z()));
    }
    return reducedPolygon;
}

QList<QPointF> Curve2D::calculateRationalDeCasteljau(QList<QPointF> polygon, int steps) {
    QList<QVector3D> newPolygon = calculateDeCasteljau(convertPolygonTo3D(polygon, rationalWeights), steps);
    return perspectiveDivide(newPolygon);
}

void Curve2D::calculateCurve() {
    addPoints(subCurvePoints(0, 1));
}

QList<QPointF> Curve2D::reducePoints(QList<QPointF> allPoints, double t) {
    QList<QPointF> reducedPoints;
    for (int i = 1; i < allPoints.length(); i++) {
        reducedPoints.append(getPointStep(allPoints[i - 1], allPoints[i], t));
    }
    return reducedPoints;
}

QList<QPointF> Curve2D::getPoints() {
    return points;
}

QPainterPath Curve2D::getCurvePath() {
    QPainterPath path;
    if(points.length() > 0) {
        path.moveTo(points[0]);
        for(int i = 1; i < points.length(); i++) {
            path.lineTo(points[i]);
        }
    }
    return path;
}

QPainterPath Curve2D::getControlPolygonPath(void) {
    return getControlPolygonPath(0);
}

QPainterPath Curve2D::getControlPolygonPath(int selectedIndex) {
    QPainterPath path;
    int i;
    QPointF tempPoint1, tempPoint2, tempPoint3, tempPoint4;
    for(i = 0; i < controlPolygon.length(); i++) {
        if(i > 0) {
            path.lineTo(controlPolygon[i]);
        }
        path.addEllipse(controlPolygon[i], 4, 4);
        path.moveTo(controlPolygon[i]);
    }
    if(closed && !controlPolygon.isEmpty()) {
        path.lineTo(controlPolygon.first());
    }
    if(selectedIndex > 0) {
        path.moveTo(controlPolygon[selectedIndex].x() - 10, controlPolygon[selectedIndex].y());
        path.lineTo(controlPolygon[selectedIndex].x() + 10, controlPolygon[selectedIndex].y());
        path.moveTo(controlPolygon[selectedIndex].x(), controlPolygon[selectedIndex].y() - 10);
        path.lineTo(controlPolygon[selectedIndex].x(), controlPolygon[selectedIndex].y() + 10);
    }
    // Used to illustrate C3 spline connection points were in the right place
    /*if(curveType == TYPE_SPLINE_2 && controlPolygon.length() > 3) {

        if(closed) {
            controlPolygon.append(controlPolygon.at(0));
            controlPolygon.append(controlPolygon.at(1));
        }
        for(i = 1; i < controlPolygon.length() - 2; i++) {
            tempPoint1 = getPointStep(controlPolygon[i], controlPolygon[i + 1], 0.3333);
            tempPoint2 = getPointStep(controlPolygon[i], controlPolygon[i + 1], 0.6666);
            tempPoint3 = getPointStep(controlPolygon[i - 1], controlPolygon[i], 0.6666);
            tempPoint4 = getPointStep(controlPolygon[i + 1], controlPolygon[i + 2], 0.3333);
            path.moveTo(tempPoint3);
            path.lineTo(tempPoint1);
            path.moveTo(tempPoint2);
            path.lineTo(tempPoint4);
        }
        if(closed) {
            controlPolygon.removeLast();
            controlPolygon.removeLast();
        }
    }*/
    return path;
}

void Curve2D::updateControlPoint(int index, QPointF newPoint) {
    controlPolygon.replace(index, newPoint);
}

void Curve2D::setType(unsigned int type) {
    curveType = type;
}

QPointF Curve2D::pointMirror(QPointF point, QPointF center) {
    return QPointF(
        center.x() + (center.x() - point.x()),
        center.y() + (center.y() - point.y())
    );
}

void Curve2D::increaseCurveDegree() {
    QList<QPointF> newControlPolygon;
    double n = (double) controlPolygon.length();
    double degree = n - 1;
    for(double i = 0; i <= n; i++) {
        if(i > 0 && i < n) {
            newControlPolygon.append(QPointF(
                 ((i / (degree + 1)) * controlPolygon[i - 1].x()) + ((1 - (i / (degree + 1))) * controlPolygon[i].x()),
                 ((i / (degree + 1)) * controlPolygon[i - 1].y()) + ((1 - (i / (degree + 1))) * controlPolygon[i].y())
            ));
        } else if(i == 0) {
            newControlPolygon.append(controlPolygon[i]);
        } else if(i == n) {
            newControlPolygon.append(controlPolygon[i - 1]);
        }
    }
    controlPolygon = newControlPolygon;
}

void Curve2D::unsetCurve() {
    x.clear();
    y.clear();
    points.clear();
    controlPolygon.clear();
}

void Curve2D::setClosed(bool isClosed) {
    closed = isClosed;
}

QList<QPointF> Curve2D::findCollisions(Curve2D curve1, Curve2D collisionCurve) {
    return findCollisions(curve1, collisionCurve, 2);
}


QList<QPointF> Curve2D::findCollisions(Curve2D curve1, Curve2D collisionCurve, double epsilon) {
    QList<QPointF> collisions;
    curve1.setDrawPointCount(15);
    collisionCurve.setDrawPointCount(15);
    QRectF origShell = curve1.getCurveShell(),
            extShell = collisionCurve.getCurveShell();
    origShell.setBottomLeft(QPointF(origShell.bottomLeft().x() - epsilon, origShell.bottomLeft().y() + epsilon));
    origShell.setTopRight(QPointF(origShell.topRight().x() + epsilon, origShell.topRight().y() + epsilon));
    extShell.setBottomLeft(QPointF(extShell.bottomLeft().x() - epsilon, extShell.bottomLeft().y() + epsilon));
    extShell.setTopRight(QPointF(extShell.topRight().x() + epsilon, extShell.topRight().y() + epsilon));
    if(!origShell.isEmpty() && !extShell.isEmpty()) {
        if(shellsCollide(origShell, extShell)) {
            QList<QPointF> currentCollidingCurves, compareAgainst, compareAgainstBuffer, currentCurveBuffer;
            QList<QRectF> currentShells, compareAgainstShells;
            currentCollidingCurves.append(QPointF(0, 1));
            compareAgainst.append(QPointF(0, 1));
            int i, y;
            double diff;
            do {
                // Lets split all curves into halves
                for(QPointF &currentDist : currentCollidingCurves) {
                    diff = fabs(currentDist.x() - currentDist.y()) / 2;
                    currentCurveBuffer.append(QPointF(currentDist.x(), currentDist.x() + diff));
                    currentCurveBuffer.append(QPointF(currentDist.y() - diff, currentDist.y()));
                }
                currentShells.clear();
                for(QPointF &distance : currentCurveBuffer) {
                    curve1.addPoints(curve1.subCurvePoints(distance.x(), distance.y()));
                    currentShells.append(curve1.getCurveShell());
                }
                currentCollidingCurves.clear();
                for(QPointF &compareAgainstCurve : compareAgainst) {
                    diff = fabs(compareAgainstCurve.x() - compareAgainstCurve.y()) / 2;
                    compareAgainstBuffer.append(QPointF(compareAgainstCurve.x(), compareAgainstCurve.x() + diff));
                    compareAgainstBuffer.append(QPointF(compareAgainstCurve.y() - diff, compareAgainstCurve.y()));
                }
                compareAgainstShells.clear();
                for(QPointF &distance : compareAgainstBuffer) {
                    collisionCurve.addPoints(collisionCurve.subCurvePoints(distance.x(), distance.y()));
                    compareAgainstShells.append(collisionCurve.getCurveShell());
                }
                compareAgainst.clear();
                for (i = 0; i < currentShells.length(); i++) {
                    for(y = 0; y < compareAgainstShells.length(); y++) {
                        if(shellsCollide(currentShells[i], compareAgainstShells[y])) {
                            if(euclideanDistance(currentShells[i].bottomRight(), currentShells[i].topLeft()) <= epsilon) {
                                collisions.append(currentShells[i].center());
                            } else {
                                currentCollidingCurves.append(currentCurveBuffer[i]);
                                // This subcurve collides with at least one another subpoint, no more search
                                break;
                            }
                        }
                    }
                }

                for (i = 0; i < compareAgainstShells.length(); i++) {
                    for(y = 0; y < currentShells.length(); y++) {
                        if(shellsCollide(compareAgainstShells[i], currentShells[y])) {
                            if(euclideanDistance(compareAgainstShells[i].bottomRight(), compareAgainstShells[i].topLeft()) <= epsilon) {
                                collisions.append(compareAgainstShells[i].center());
                            } else {
                                compareAgainst.append(compareAgainstBuffer[i]);
                                // This subcurve collides with at least one another subpoint, no more search
                                break;
                            }
                        }
                    }
                }

                currentCurveBuffer.clear();
                compareAgainstBuffer.clear();
            } while (currentCollidingCurves.length() > 0);
        }
    }
    return collisions;
}

QRectF Curve2D::getCurveShell(void) {
    if(points.length() > 1) {
        double xmin = INFINITY,
               xmax = -1 * INFINITY,
               ymin = INFINITY,
               ymax = -1 * INFINITY;
        for(int i = 0; i < points.length(); i++) {
            if(points[i].x() > xmax) {
                xmax = points[i].x();
            }
            if(points[i].x() < xmin) {
                xmin = points[i].x();
            }
            if(points[i].y() > ymax) {
                ymax = points[i].y();
            }
            if(points[i].y() < ymin) {
                ymin = points[i].y();
            }
        }
        return QRectF(QPointF(xmin, ymin), QPointF(xmax, ymax));
    } else {
        return QRectF();
    }
}

bool Curve2D::shellsCollide(QRectF shell1, QRectF shell2) {
    //return shell1.intersects(shell2);
    return (shell1.topLeft().x() <= shell2.bottomRight().x() &&
            shell1.bottomRight().x() >= shell2.topLeft().x() &&
            shell1.topLeft().y() <= shell2.bottomRight().y() &&
            shell1.bottomRight().y() >= shell2.topLeft().y());
}

bool Curve2D::curvesCollide(Curve2D collisionCurve) {
    return shellsCollide(getCurveShell(), collisionCurve.getCurveShell());
}

Curve2D Curve2D::subCurve(bool firstHalf) {
    Curve2D newCurve;
    int midPoint = (int) (points.length() / 2);
    if(firstHalf) {
        newCurve.addPoints(points.mid(0, midPoint));
    } else {
        newCurve.addPoints(points.mid(midPoint));
    }
    return newCurve;
}

QList<QPointF> Curve2D::CROpenFirstSubControlPolygon(QPointF p0, QPointF p1, QPointF p2) {
    QList<QPointF> subControlPolygon;
    QPointF l1 = CRDerivative(p0, p2);
    subControlPolygon.append(p0);
    subControlPolygon.append(QPointF(
         p1.x() - (0.5 * (l1.x())),
         p1.y() - (0.5 * (l1.y()))
    ));
    subControlPolygon.append(p1);
    return subControlPolygon;
}

QList<QPointF> Curve2D::CROpenLastSubControlPolygon(QPointF p0, QPointF p1, QPointF p2) {
    QList<QPointF> subControlPolygon;
    QPointF l1 = CRDerivative(p0, p2);
    subControlPolygon.append(p1);
    subControlPolygon.append(QPointF(
         p1.x() + (0.5 * (l1.x())),
         p1.y() + (0.5 * (l1.y()))
    ));
    subControlPolygon.append(p2);
    return subControlPolygon;
}

QList<QPointF> Curve2D::CRSubControlPolygon(QPointF p1, QPointF p2, QPointF p3, QPointF p4) {
    QList<QPointF> subControlPolygon;
    QPointF l2 = CRDerivative(p1, p3);
    QPointF l3 = CRDerivative(p2, p4);
    subControlPolygon.append(p2);
    subControlPolygon.append(QPointF(
        p2.x() + (l2.x() / 3),
        p2.y() + (l2.y() / 3)
    ));
    subControlPolygon.append(QPointF(
        p3.x() - (l3.x() / 3),
        p3.y() - (l3.y() / 3)
    ));
    subControlPolygon.append(p3);
    return subControlPolygon;
}

QPointF Curve2D::getBOMidPoint(QPointF z0, QPointF _b0, QPointF _b4, QPointF b4) {
    QList<QPointF> pointList = BOSubControlPolygon(z0, _b0, _b4, b4);
    return getPointStep(pointList[1], pointList[2], 0.5);
}

QList<QPointF> Curve2D::ISSubControlPolygon(QPointF z0, QPointF _b0, QPointF _b4, QPointF b4, QPointF z1) {
    QPointF _b2 = getBOMidPoint(z0, _b0, _b4, b4),
            b2 = getBOMidPoint(_b0, _b4, b4, z1);
    double deltaI = euclideanDistance(b4, _b4),
           deltaI_1 = euclideanDistance(_b0, _b4);
    double beta = deltaI / deltaI_1;
    double alfa = 1.0 / beta;
    double  a0 = (-1 * ((beta * beta) / (2 * (1 + beta)))),
            a1 = ((1 + beta) / 2),
            a2 = (1 / (2 * (1 + beta))),
            _a0 = (1 / (2 * (1 + alfa))),
            _a1 = ((1 + alfa) / 2),
            _a2 = (-1 * ((alfa * alfa) / (2 * (1 + alfa))));
    QList<QPointF> subPolygon;
    subPolygon.append(((_a0 * _b2) + (_a1 * _b4) + (_a2 * b2)));
    subPolygon.append(_b4);
    subPolygon.append(((a0 * _b2) + (a1 * _b4) + (a2 * b2)));
    return subPolygon;
}

QPointF Curve2D::CRDerivative(QPointF p1, QPointF p3) {
    return QPointF(
        0.5 * (p3.x() - p1.x()),
        0.5 * (p3.y() - p1.y())
    );
}

QPointF Curve2D::BODerivative(QPointF p, QPointF pPrevious, QPointF pNext, double delta, double deltaPrevious) {
    double coef1 = ((delta - deltaPrevious) / (deltaPrevious * delta)),
            coef2 = (deltaPrevious / (delta * (deltaPrevious + delta))),
            coef3 = (delta / (deltaPrevious * (deltaPrevious + delta)));
    return QPointF(
        ((coef1 * p.x()) + (coef2 * pNext.x()) - (coef3 * pPrevious.x())),
        ((coef1 * p.y()) + (coef2 * pNext.y()) - (coef3 * pPrevious.y()))
    );
}

double Curve2D::BOGetDistance(QPointF p1, QPointF p2) {
    switch (distanceMetric) {
        case DISTANCE_EUCLIDEAN:
            return pow(euclideanDistance(p1, p2), distanceAlfa);
        break;

        case DISTANCE_CITY_BLOCK:
            return pow(cityBlockDistance(p1, p2), distanceAlfa);
        break;

        case DISTANCE_CHEBYSHEV:
            return pow(chebyshevDistance(p1, p2), distanceAlfa);
        break;

    }
}

QList<QPointF> Curve2D::BOOpenFirstSubControlPolygon(QPointF p0, QPointF p1, QPointF p2) {
    QList<QPointF> newPolygon;
    double delta0 = BOGetDistance(p0, p1),
            delta1 = BOGetDistance(p1, p2);
    QPointF l1 = BODerivative(p1, p0, p2, delta1, delta0);
    newPolygon.append(p0);
    newPolygon.append(QPointF(
        p1.x() - (delta0 / 2) * l1.x(),
        p1.y() - (delta0 / 2) * l1.y()
    ));
    newPolygon.append(p1);
    return newPolygon;
}

QList<QPointF> Curve2D::BOOpenLastSubControlPolygon(QPointF p0, QPointF p1, QPointF p2) {
    QList<QPointF> newPolygon;
    double delta0 = BOGetDistance(p0, p1),
            delta1 = BOGetDistance(p1, p2);
    QPointF l1 = BODerivative(p1, p0, p2, delta1, delta0);
    newPolygon.append(p1);
    newPolygon.append(QPointF(
        p1.x() + (delta1 / 2) * l1.x(),
        p1.y() + (delta1 / 2) * l1.y()
    ));
    newPolygon.append(p2);
    return newPolygon;
}


QList<QPointF> Curve2D::BOSubControlPolygon(QPointF p1, QPointF p2, QPointF p3, QPointF p4) {
    QList<QPointF> newPolygon;
    double delta1 = BOGetDistance(p1, p2),
            delta2 = BOGetDistance(p2, p3),
            delta3 = BOGetDistance(p3, p4);
    QPointF l2 = BODerivative(p2, p1, p3, delta2, delta1);
    QPointF l3 = BODerivative(p3, p2, p4, delta3, delta2);
    newPolygon.append(p2);
    newPolygon.append(QPointF(
        p2.x() + ((delta2 / 3) * l2.x()),
        p2.y() + ((delta2 / 3) * l2.y())
    ));
    newPolygon.append(QPointF(
        p3.x() - ((delta2 / 3) * l3.x()),
        p3.y() - ((delta2 / 3) * l3.y())
    ));
    newPolygon.append(p3);
    return newPolygon;
}

QList<QPointF> Curve2D::subCurvePoints(double tStart, double tStop) {
    QList<QPointF> calculatedPoints;
    QPointF startMirrorPoint, endMirrorPoint;
    if(controlPolygon.length() > 1) {
        QList<QPointF> subControlPoints;
        int cpLength;
        int i;
        x.clear();
        y.clear();
        points.clear();
        if(closed) {
            controlPolygon.append(controlPolygon.first());
        }
        switch(curveType) {

        case TYPE_BEZIER:
            calculatedPoints = calculateBezierCurve(controlPolygon, drawPointCount, tStart, tStop);
        break;

        case TYPE_SPLINE:
            if(controlPolygon.length() > 2) {
                if(closed) {
                    for(i = 1; i < controlPolygon.length(); i++) {
                        subControlPoints.clear();
                        subControlPoints.append(getPointStep(controlPolygon[i - 1], controlPolygon[i], 0.5));
                        subControlPoints.append(controlPolygon[i]);
                        if(i == (controlPolygon.length() - 1)) {
                            subControlPoints.append(getPointStep(controlPolygon[i], controlPolygon[1], 0.5));
                        } else {
                            subControlPoints.append(getPointStep(controlPolygon[i], controlPolygon[i + 1], 0.5));
                        }
                        calculatedPoints += calculateBezierCurve(subControlPoints, drawPointCount, tStart, tStop);
                    }
                } else {
                    // Add first phantom control point
                    QPointF firstOld = controlPolygon.first(),
                            lastOld = controlPolygon.last();
                    controlPolygon.removeFirst();
                    controlPolygon.removeLast();
                    controlPolygon.prepend(pointMirror(controlPolygon.first(), firstOld));
                    controlPolygon.append(pointMirror(controlPolygon.last(), lastOld));
                    for(i = 1; i < controlPolygon.length() - 1; i++) {
                        subControlPoints.clear();
                        subControlPoints.append(getPointStep(controlPolygon[i - 1], controlPolygon[i], 0.5));
                        subControlPoints.append(controlPolygon[i]);
                        subControlPoints.append(getPointStep(controlPolygon[i], controlPolygon[i + 1], 0.5));
                        calculatedPoints += calculateBezierCurve(subControlPoints, drawPointCount, tStart, tStop);
                    }
                    controlPolygon.removeFirst();
                    controlPolygon.removeLast();
                    controlPolygon.prepend(firstOld);
                    controlPolygon.append(lastOld);
                }
            } else {
                calculatedPoints = calculateBezierCurve(controlPolygon, drawPointCount, tStart, tStop);
            }
        break;

        case TYPE_SPLINE_2:
            // Add first phantom control point
            if(controlPolygon.length() > 3) {
                QPointF firstOld = controlPolygon.first(),
                        lastOld = controlPolygon.last(),
                        tempPoint1, tempPoint2, tempPoint3, tempPoint4;
                if(closed) {
                    controlPolygon.append(controlPolygon.at(1));
                    controlPolygon.append(controlPolygon.at(2));
                } else {
                    controlPolygon.prepend(pointMirror(controlPolygon.first(), firstOld));
                    controlPolygon.prepend(pointMirror(firstOld, controlPolygon.first()));
                    controlPolygon.append(pointMirror(controlPolygon.last(), lastOld));
                    controlPolygon.append(pointMirror(lastOld, controlPolygon.last()));
                }

                for(i = 1; i < controlPolygon.length() - 2; i++) {
                    subControlPoints.clear();
                    tempPoint1 = getPointStep(controlPolygon[i], controlPolygon[i + 1], 0.3333);
                    tempPoint2 = getPointStep(controlPolygon[i], controlPolygon[i + 1], 0.6666);
                    tempPoint3 = getPointStep(controlPolygon[i - 1], controlPolygon[i], 0.6666);
                    tempPoint4 = getPointStep(controlPolygon[i + 1], controlPolygon[i + 2], 0.3333);
                    subControlPoints.append(getPointStep(tempPoint1, tempPoint3, 0.5));
                    subControlPoints.append(tempPoint1);
                    subControlPoints.append(tempPoint2);
                    subControlPoints.append(getPointStep(tempPoint2, tempPoint4, 0.5));
                    calculatedPoints += calculateBezierCurve(subControlPoints, drawPointCount, tStart, tStop);
                }
                if(closed) {
                    controlPolygon.removeLast();
                    controlPolygon.removeLast();
                } else {
                    controlPolygon.removeFirst();
                    controlPolygon.removeLast();
                    controlPolygon.removeFirst();
                    controlPolygon.removeLast();
                }

            } else {
                calculatedPoints = calculateBezierCurve(controlPolygon, drawPointCount, tStart, tStop);
            }
        break;
        case TYPE_CATMULL_ROM:

            if(closed) {
                controlPolygon.append(controlPolygon.at(1));
                controlPolygon.append(controlPolygon.at(2));
                controlPolygon.append(controlPolygon.at(3));
            }
            cpLength = controlPolygon.length() + (closed ? -4 : 0);
            for(int i = 0; i < (closed ? cpLength : cpLength - 1); i++) {
                if(!closed && i == 0) {
                    subControlPoints = CROpenFirstSubControlPolygon(
                        controlPolygon[getListIndex(i, cpLength)],
                        controlPolygon[getListIndex(i + 1, cpLength)],
                        controlPolygon[getListIndex(i + 2, cpLength)]
                    );
                } else if (!closed && i == (cpLength - 2)) {
                    subControlPoints = CROpenLastSubControlPolygon(
                        controlPolygon[getListIndex(i - 1, cpLength)],
                        controlPolygon[getListIndex(i, cpLength)],
                        controlPolygon[getListIndex(i + 1, cpLength)]
                    );
                } else {
                    subControlPoints = CRSubControlPolygon(
                        controlPolygon[getListIndex(i - 1, cpLength)],
                        controlPolygon[getListIndex(i, cpLength)],
                        controlPolygon[getListIndex(i + 1, cpLength)],
                        controlPolygon[getListIndex(i + 2, cpLength)]
                    );
                }
                calculatedPoints += calculateBezierCurve(
                    subControlPoints,
                    drawPointCount,
                    tStart,
                    tStop
                );
            }
            if(closed) {
                controlPolygon.removeLast();
                controlPolygon.removeLast();
                controlPolygon.removeLast();
            }

        break;
        case TYPE_BESSEL_OVERHAUSER:
            if(closed) {
                controlPolygon.append(controlPolygon.at(1));
                controlPolygon.append(controlPolygon.at(2));
                controlPolygon.append(controlPolygon.at(3));
            }
            cpLength = controlPolygon.length() + (closed ? -4 : 0);
            for(int i = 0; i < (closed ? cpLength : cpLength - 1); i++) {
                if(!closed && i == 0) {
                    subControlPoints = BOOpenFirstSubControlPolygon(
                        controlPolygon[getListIndex(i, cpLength)],
                        controlPolygon[getListIndex(i + 1, cpLength)],
                        controlPolygon[getListIndex(i + 2, cpLength)]
                    );
                } else if (!closed && i == (cpLength - 2)) {
                    subControlPoints = BOOpenLastSubControlPolygon(
                        controlPolygon[getListIndex(i - 1, cpLength)],
                        controlPolygon[getListIndex(i, cpLength)],
                        controlPolygon[getListIndex(i + 1, cpLength)]
                    );
                } else {
                    subControlPoints = BOSubControlPolygon(
                        controlPolygon[getListIndex(i - 1, cpLength)],
                        controlPolygon[getListIndex(i, cpLength)],
                        controlPolygon[getListIndex(i + 1, cpLength)],
                        controlPolygon[getListIndex(i + 2, cpLength)]
                    );
                }
                calculatedPoints += calculateBezierCurve(
                    subControlPoints,
                    drawPointCount,
                    tStart,
                    tStop
                );
            }
            if(closed) {
                controlPolygon.removeLast();
                controlPolygon.removeLast();
                controlPolygon.removeLast();
            }
        break;

        case TYPE_CHAIKIN:
            calculatedPoints = controlPolygon;
            if(closed && calculatedPoints.length() > 1) {
                calculatedPoints.append(calculatedPoints.at(1));
            }

            for(int i = 0; i < algorithmSteps; i++) {
                if(!closed && controlPolygon.length() > 1) {
                    calculatedPoints.replace(0,
                                             pointMirror(getPointStep(calculatedPoints.first(), calculatedPoints.at(1), (0.3333)), controlPolygon.first())
                                             );
                    calculatedPoints.replace((calculatedPoints.length() - 1),
                                             pointMirror(getPointStep(calculatedPoints.last(), calculatedPoints.at((calculatedPoints.length() - 2)), (0.3333)), controlPolygon.last())
                                             );
                }
                calculatedPoints = ChaikinDividePolygon(calculatedPoints);
            }
        break;

        case TYPE_CHAIKIN_2:
            calculatedPoints = controlPolygon;
            if(calculatedPoints.length() > 2) {
                if(closed) {
                    calculatedPoints.append(calculatedPoints.at(1));
                    calculatedPoints.append(calculatedPoints.at(2));
                    calculatedPoints.append(calculatedPoints.at(3));
                } else {
                    calculatedPoints.prepend(controlPolygon.first());
                    calculatedPoints.prepend(pointMirror(calculatedPoints.at(1), controlPolygon.first()));
                    calculatedPoints.append(controlPolygon.last());
                }
                for(int i = 0; i < algorithmSteps; i++) {
                    if(!closed) {
                        calculatedPoints.prepend(pointMirror(calculatedPoints.first(), controlPolygon.first()));
                        calculatedPoints.append(pointMirror(calculatedPoints.last(), controlPolygon.last()));
                    }
                    calculatedPoints = ChaikinDividePolygon3rd(calculatedPoints);
                }
            }
        break;

        case TYPE_RATIONAL_BEZIER:
            if(rationalDeCasteljau) {
                calculatedPoints = calculateRationalDeCasteljau(controlPolygon, algorithmSteps);
            } else {
                calculatedPoints = calculateRationalBezier(controlPolygon, 20);
            }
        break;

        case TYPE_INTERPOLATING:
            if(closed) {
                controlPolygon.append(controlPolygon.at(1));
                controlPolygon.append(controlPolygon.at(2));
                controlPolygon.append(controlPolygon.at(3));
            }
            cpLength = controlPolygon.length();

            int i;
            for(i = 0; i < cpLength; i++) {
                subControlPoints += ISSubControlPolygon(
                            controlPolygon[getListIndex(i - 2, cpLength)],
                            controlPolygon[getListIndex(i - 1, cpLength)],
                            controlPolygon[getListIndex(i, cpLength)],
                            controlPolygon[getListIndex(i + 1, cpLength)],
                            controlPolygon[getListIndex(i + 2, cpLength)]
                            );
            }
            subControlPoints.removeFirst();
            subControlPoints.removeLast();
            QList<QPointF> subPolygon;
            for(i = 0; i < cpLength - 1; i++) {
                subPolygon.clear();
                subPolygon.append(subControlPoints[i * 3]);
                subPolygon.append(subControlPoints[(i * 3) + 1]);
                subPolygon.append(subControlPoints[(i * 3) + 2]);
                subPolygon.append(subControlPoints[(i * 3) + 3]);
                calculatedPoints += calculateBezierCurve(
                            subPolygon,
                            drawPointCount,
                            tStart,
                            tStop
                        );
            }
            if(closed) {
                controlPolygon.removeLast();
                controlPolygon.removeLast();
                controlPolygon.removeLast();
            }
        break;

        }

        if(closed) {
            controlPolygon.removeLast();
        }
        isCalculated = true;
    }
    return calculatedPoints;
}

int Curve2D::getListIndex(int index, int listLength) {
    if (index < 0) {
        return (listLength + index);
    } else if (index >= listLength) {
        return (index - listLength);
    } else {
        return index;
    }
}

void Curve2D::setAlgorithmSteps(unsigned int stepCount) {
    algorithmSteps = stepCount;
}

QList<QPointF> Curve2D::ChaikinDividePolygon(QList<QPointF> curvePolygon) {
    QList<QPointF> newPolygon;
    for(int i = 0; i < (curvePolygon.length() - 1); i++) {
        newPolygon.append((0.75 * curvePolygon[i]) + (0.25 * curvePolygon[i + 1]));
        newPolygon.append((0.25 * curvePolygon[i]) + (0.75 * curvePolygon[i + 1]));
    }
    return newPolygon;
}

QList<QPointF> Curve2D::ChaikinDividePolygon3rd(QList<QPointF> curvePolygon) {
    QList<QPointF> newPolygon;
    for(int i = 1; i < (curvePolygon.length() - 1); i++) {
        newPolygon.append((0.125 * curvePolygon[i - 1]) + (0.75 * curvePolygon[i]) + (0.125 * curvePolygon[i + 1]));
        newPolygon.append((0.5 * curvePolygon[i]) + (0.5 * curvePolygon[i + 1]));
    }
    return newPolygon;
}

void Curve2D::removeLastControlPoint(void) {
    controlPolygon.removeLast();
    rationalWeights.removeLast();
}


void Curve2D::setRationalWeight(double weight) {
    rationalWeight = weight;
    for(int i = 1; i < rationalWeights.length(); i++) {
        rationalWeights[i] = weight;
    }
}

void Curve2D::setRationalWeight(int index, double weight) {
    rationalWeights[index] = weight;
}

unsigned int Curve2D::getType(void) {
    return curveType;
}

double Curve2D::getRationalWeight(int index) {
    return rationalWeights.at(index);
}

void Curve2D::setRationalDeCasteljau(bool enabled) {
    rationalDeCasteljau = enabled;
}

