#-------------------------------------------------
#
# Project created by QtCreator 2015-03-17T15:38:59
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CurvesDemo
TEMPLATE = app

CONFIG += static

QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    curve2d.cpp \
    drawingarea.cpp \
    utilities.cpp

HEADERS  += mainwindow.h \
    curve2d.h \
    drawingarea.h \
    utilities.h

FORMS    += mainwindow.ui

RESOURCES += \
    resources.qrc
