#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->showMaximized();
}

MainWindow::~MainWindow() {
    delete ui;
}


void MainWindow::forceControlPolygon(void) {
    if(!ui->checkBox->isChecked()) {
        ui->checkBox->setChecked(true);
        ui->canvas->setPolygonVisibility(true);
    }
}

void MainWindow::on_checkBox_clicked(bool checked) {
    ui->canvas->setPolygonVisibility(checked);
}

void MainWindow::on_checkBox_2_clicked(bool checked) {
    ui->canvas->setCurveClosed(checked);
}

void MainWindow::resetControlsForDrawing(bool enableAlgSteps, bool enableWeightSlider) {
    forceControlPolygon();
    if(ui->checkBox_2->isChecked()) {
        ui->checkBox_2->setChecked(false);
        ui->canvas->setCurveClosed(false);
    }
    ui->complete_draw->setEnabled(true);
    ui->checkBox_2->setEnabled(true);
    ui->canvas->setAlgorithmSteps(5);
    ui->algorithmStepsInput->setValue(5);
    ui->algorithmStepsInput->setEnabled(enableAlgSteps);
    ui->rationalWeightInput->setValue(50);
    ui->rationalWeightInput->setEnabled(enableWeightSlider);
}

void MainWindow::executeCurveDraw(unsigned int curveType) {
    resetControlsForDrawing((curveType == Curve2D::TYPE_CHAIKIN), false);
    drawingActive = true;
    ui->complete_draw->setText("Show curve");
    ui->canvas->drawCurve(curveType);
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    if(ui->canvas->drawingActive()) {
        if(event->key() == Qt::Key_Escape || event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return) {
            ui->complete_draw->click();
        }
    }
}

void MainWindow::on_draw_c2_spline_clicked() {
    executeCurveDraw(Curve2D::TYPE_SPLINE);
}

void MainWindow::on_draw_c3_spline_clicked() {
    executeCurveDraw(Curve2D::TYPE_SPLINE_2);
}

void MainWindow::on_draw_bo_spline_clicked() {
    executeCurveDraw(Curve2D::TYPE_BESSEL_OVERHAUSER);
}

void MainWindow::on_draw_chaikin_clicked() {
    executeCurveDraw(Curve2D::TYPE_CHAIKIN);
}

void MainWindow::on_draw_rational_bezier_clicked() {
    resetControlsForDrawing(false, true);
    ui->complete_draw->setEnabled(false);
    ui->checkBox_2->setEnabled(false);
    ui->canvas->drawRationalDemo();
}

void MainWindow::on_complete_draw_clicked() {
    if(drawingActive) {
        ui->canvas->stopDrawingMode();
        ui->complete_draw->setText("Edit curve");
        drawingActive = false;
    } else {
        ui->canvas->editCurve();
        forceControlPolygon();
        ui->complete_draw->setText("Show curve");
        drawingActive = true;
    }

}

void MainWindow::on_algorithmStepsInput_valueChanged(int arg1) {
    ui->canvas->setAlgorithmSteps((double) arg1);
}

void MainWindow::on_rationalWeightInput_valueChanged(int value) {
    if(value < 50) {
        ui->canvas->setRationalWeight((double) value / 50);
    } else {
        ui->canvas->setRationalWeight((((double) value - 50) / 5) + 1);
    }
}
