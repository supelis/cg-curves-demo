#include "drawingarea.h"
#include <iostream>
#include "utilities.h"
#include <QApplication>

DrawingArea::DrawingArea(QWidget *parent)
    : QWidget(parent)
{
    setBackgroundRole(QPalette::Base);
    setAutoFillBackground(true);
   // Curve2D curve;
    QPointF start(-88, -87), stop(56, -31);
    draggedPointIndex = -1;
    curve.setType(Curve2D::TYPE_SPLINE);
    curve.setStart(start);
    curve.setEnd(stop);
    curve.addControlPoint(QPointF(-150, 50), false);
    curve.addControlPoint(QPointF(-114, 64), false);
    curve.calculateCurve();
    addControlPointActive = false;
    polygonVisibility = false;
    curveDrawingActive = false;
    collisionMode = false;
    setMouseTracking(true);
    mirrorCenter = QPointF(0, 0);
    mousePos = QPointF(0, 0);

}

void DrawingArea::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    painter.fillRect(QRect(0, 0, 500, 500), Qt::white);

    int w_2 = width() / 2;
    int h_2 = height() / 2;
    painter.setRenderHint(QPainter::Antialiasing);

    painter.setPen( Qt::blue );
    painter.drawLine( 0, h_2, width(), h_2);     // X-Axis
    painter.drawLine( w_2, 0 , w_2, height() );  // Y-Axis

    painter.setPen(Qt::black);

    QMatrix m;
    m.translate( w_2, h_2 );
    m.scale( 1, -1 );
    painter.setMatrix( m );



    transfMatrix = m;


    if(polygonVisibility) {
        painter.setPen( Qt::red );
        painter.setBrush( Qt::green );
        painter.drawPath(curve.getControlPolygonPath(selectedPointIndex));
    }
    if(curveDrawingActive) {
        QList<QPointF> polygonPoints = curve.getControlPoints();
        if(polygonPoints.length() > 0) {
            painter.setPen( Qt::gray );
            painter.drawLine(mousePos, polygonPoints.last());
            painter.setPen( Qt::red );
            painter.drawEllipse(mousePos, 4, 4);
        }
    } else if (curveEditingActive) {
        painter.setPen( Qt::gray );
        QList<QPointF> nearestPoints = curve.getNearestPoints(mousePos);
        if(nearestPoints.length() == 2) {
            painter.drawLine(mousePos, nearestPoints.first());
            painter.drawLine(mousePos, nearestPoints.last());
        } else {
            painter.drawLine(mousePos, nearestPoints.last());
        }
        painter.setPen( Qt::red );
        painter.drawEllipse(mousePos, 4, 4);
    } else {
        painter.setPen( Qt::black );
        painter.setBrush( Qt::transparent );
        painter.drawPath(curve.getCurvePath());
    }
    if(collisionMode) {
        if(collisionCurve.isCalculated) {
            painter.setPen( Qt::black );
            painter.setBrush( Qt::transparent );
            painter.drawPath(collisionCurve.getCurvePath());
            if(polygonVisibility) {
                painter.setPen( Qt::red );
                painter.setBrush( Qt::green );
                painter.drawPath(collisionCurve.getControlPolygonPath());
            }
            QList<QPointF> collisionPoints = curve.findCollisions(curve, collisionCurve, 0.5);
            for (QPointF &collisionPoint : collisionPoints) {
                painter.setPen( Qt::blue );
                painter.setBrush( Qt::blue );
                painter.drawEllipse(collisionPoint, 2, 2);
            }
        }
    }
}

QPointF DrawingArea::transformPointCoords(QPointF point) {
    QPointF transformedPoint = transfMatrix.map(point);
    transformedPoint.setX(transformedPoint.x() - width());
    return transformedPoint;
}


QPointF DrawingArea::transformPointCoords(QPoint point) {
    return transformPointCoords(QPointF(point.x(), point.y()));
}

void DrawingArea::mouseReleaseEvent(QMouseEvent *event) {
    draggedPointIndex = -1;
    QApplication::restoreOverrideCursor();
}

void DrawingArea::addControlPoint() {
    addControlPointActive = true;
}

void DrawingArea::setPolygonVisibility(bool visible) {
    polygonVisibility = visible;
    repaint();
}

void DrawingArea::drawCurve(unsigned int curveType) {
    if(curveDrawingActive) {
        stopDrawingMode();
    }
    curve.unsetCurve();
    curve.setType(curveType);
    repaint();
    curveDrawingActive = true;
    curveDrawingPoint = 0;
    this->setStatusTip("When drawing a cruve, click `Esc` or `Enter` to complete drawing.");
}

void DrawingArea::mouseMoveEvent(QMouseEvent *event) {
    QPointF newPoint = transformPointCoords(event->pos());
    mousePos = newPoint;
    if(curveDrawingActive || curveEditingActive) {
        if(!QApplication::overrideCursor()) {
            QApplication::setOverrideCursor(Qt::CrossCursor);
        }
        if(curveDrawingPoint == 0) {
            curve.setStart(newPoint);
        }/* else if (curveDrawingPoint) {
            curve.setEnd(newPoint);
        }*/
        //curve.calculateCurve();
        update();
        return;
    } else if(draggedPointIndex > -1) {
        curve.updateControlPoint(draggedPointIndex, newPoint);
        curve.calculateCurve();
        update();
    } else {
        QList<QPointF> points = curve.getControlPoints();
        bool overidden = false;
        for(int i = 0; i < points.length(); i++) {
            if(checkPointClick(newPoint, points[i], 6)) {
                QApplication::setOverrideCursor(Qt::OpenHandCursor);
                overidden = true;
                break;
            }
        }
        if(!overidden) {
            QApplication::restoreOverrideCursor();
        }
    }

}

void DrawingArea::editCurve(void) {
    curveEditingActive = true;
}

void DrawingArea::stopDrawingMode() {
    this->setStatusTip("");
    curveDrawingActive = false;
    curveEditingActive = false;
    curveDrawingPoint = -1;
    curve.calculateCurve();
    repaint();
}

void DrawingArea::mousePressEvent(QMouseEvent *event) {
    selectedPointIndex = 0;
    if(curveDrawingActive) {
        if(curveDrawingPoint == 0) {
            curveDrawingPoint++;
        } else {
            curve.appendControlPoint(transformPointCoords(event->pos()));
        }
    } else {
        QPointF transformedPoint = transformPointCoords(event->pos());
        if(addControlPointActive || curveEditingActive) {
            addControlPointActive = false;
            curve.addControlPoint(transformedPoint);
            curve.calculateCurve();
            repaint();
        } else if (polygonVisibility) {
            QList<QPointF> points = curve.getControlPoints();
            for(int i = 0; i < points.length(); i++) {
                if(checkPointClick(transformedPoint, points[i], 6)) {
                    QApplication::setOverrideCursor(Qt::ClosedHandCursor);
                    draggedPointIndex = i;
                    /*if(i > 0 && i < (points.length() - 1) && curve.getType() == Curve2D::TYPE_RATIONAL_BEZIER) {
                        selectedPointIndex = i;
                        emit changedSliderValue(curve.getRationalWeight(i));
                    }*/
                    break;
                }
            }
        }
    }
    repaint();
}

bool DrawingArea::drawingActive(void) {
    return (curveDrawingActive || curveEditingActive);
}

void DrawingArea::setCurveClosed(bool closed) {
    curve.setClosed(closed);
    curve.calculateCurve();
    repaint();
}

void DrawingArea::setCurveDistanceMetric(unsigned int metric) {
    curve.setDistanceMetric(metric);
    curve.calculateCurve();
    repaint();
}

void DrawingArea::setCurveDistanceAlfa(double alfa) {
    curve.setDistanceAlfa(alfa);
    curve.calculateCurve();
    repaint();
}

void DrawingArea::increaseCurveDegree() {
    curve.increaseCurveDegree();
    curve.calculateCurve();
    repaint();
}

void DrawingArea::addCollisionCurve() {
    collisionCurve = curve;
    curve.unsetCurve();
    collisionMode = true;
}

void DrawingArea::setAlgorithmSteps(double stepCount) {
    curve.setAlgorithmSteps((unsigned int) stepCount);
    curve.calculateCurve();
    repaint();
}

void DrawingArea::setRationalWeight(double weight) {
    if(selectedPointIndex > 0) {
        curve.setRationalWeight(selectedPointIndex, weight);
    } else {
        curve.setRationalWeight(weight);
    }
    curve.calculateCurve();
    repaint();
}

void DrawingArea::setRationalDeCasteljau(int enabled) {
    curve.setRationalDeCasteljau((bool) enabled);
    curve.calculateCurve();
    repaint();
}

void DrawingArea::drawRationalDemo(void) {
    // Stop drawing mode in case it was already active
    if(curveDrawingActive) {
        stopDrawingMode();
    }
    curve.unsetCurve();
    curve.setType(Curve2D::TYPE_RATIONAL_BEZIER);
    curve.setRationalDeCasteljau(true);
    curve.setStart(QPointF(-100, -100));
    curve.setEnd(QPointF(100, -100));
    curve.addControlPoint(QPointF(0, 200), false);
    curve.calculateCurve();
    repaint();
}
