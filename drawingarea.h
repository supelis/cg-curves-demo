#ifndef DRAWINGAREA_H
#define DRAWINGAREA_H

#include <QWidget>
#include <QPainter>
#include "mainwindow.h"
#include <QPaintEvent>
#include "curve2d.h"

class DrawingArea : public QWidget
{
    Q_OBJECT
protected:
    int draggedPointIndex;
    int selectedPointIndex = 0;
    int curveDrawingPoint = -1;
    bool collisionMode;
    bool addControlPointActive;
    bool polygonVisibility;
    bool curveDrawingActive;
    bool curveEditingActive = false;
    QPointF mousePos;
    QPointF mirrorCenter;
    Curve2D curve;              // Current Active Curve
    Curve2D collisionCurve;
    QTransform worldTransform;
    QMatrix transfMatrix;
    QPointF transformPointCoords(QPoint);
    QPointF transformPointCoords(QPointF);
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

public:
    DrawingArea(QWidget *parent);
    void addControlPoint(void);
    void setPolygonVisibility(bool visible);
    void drawCurve(unsigned int curveType);
    void drawRationalDemo(void);
    void setCurveClosed(bool closed);
    void addCollisionCurve(void);
    void increaseCurveDegree(void);
    void setCurveDistanceMetric(unsigned int metric);
    void setCurveDistanceAlfa(double alfa);
    void setAlgorithmSteps(double stepCount);
    void stopDrawingMode(void);
    void setRationalWeight(double weight);
    void setRationalDeCasteljau(int enabled);
    void editCurve(void);
    bool drawingActive(void);

signals:
    void changedSliderValue(double value);


};


#endif // DRAWINGAREA_H
